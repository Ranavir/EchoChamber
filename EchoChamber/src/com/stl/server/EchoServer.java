package com.stl.server;

import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

/** 
 * @ServerEndpoint gives the relative name for the end point
 * This will be accessed via ws://localhost:8080/EchoChamber/echo
 * Where "localhost" is the address of the host,
 * "EchoChamber" is the name of the package
 * and "echo" is the address to access this class from the server
 */
//@ServerEndpoint(value="/echo", configurator=ServletAwareConfig.class)
@ServerEndpoint(value="/echo") 
public class EchoServer {
	static String msg = "" ;
	static String host ="" ;
	//static String ipAddr ="" ;
	 //private EndpointConfig config;
	 private HttpSession httpSession;
    /**
     * @OnOpen allows us to intercept the creation of a new session.
     * The session class allows us to send data to the user.
     * In the method onOpen, we'll let the user know that the handshake was 
     * successful.
     */
    @OnOpen
    //public void onOpen(Session session,EndpointConfig config){
    public void onOpen(Session session){
    	httpSession = ((PrincipalWithSession) session.getUserPrincipal()).getSession();
    	
    	//this.config = config;
    	/*Set set = config.getUserProperties().keySet();
    	Iterator<String> it = set.iterator();
        while(it.hasNext()){
           System.out.println(it.next());
        }*/
    	//host = config.getUserProperties().get("host").toString() ;
    	String ipAddr = httpSession.getAttribute("remoteHost").toString();
    	System.out.println("Opened Connection with Ip : "+ipAddr+" with Session Id: "+httpSession.getId());
        //System.out.println(session.getId() + " has opened a connection");
    	//System.out.println(host + " has opened a connection");
        try {
//        	session.getBasicRemote().sendText("Connection Established");
        	session.getBasicRemote().sendText("Connection to IP :"+ipAddr+" Established with Session Id :"+httpSession.getId());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
 
    /**
     * When a user sends a message to the server, this method will intercept the message
     * and allow us to react to it. For now the message is read as a String.
     */
    @OnMessage
    public void onMessage(String message, Session session){
        //System.out.println("Message from " + session.getId() + ": " + message);
    	/*HttpSession httpSession = (HttpSession) config.getUserProperties().get("httpSession");
        ServletContext servletContext = httpSession.getServletContext();*/
    	String ipAddr = httpSession.getAttribute("remoteHost").toString();
    	System.out.println(msg);
        //msg = "<br>Message from ID "+session.getId() + ": " + message + msg ;
    	msg = "<br>Message from IP "+ipAddr+ ": " + message + msg ;
        try {
            //session.getBasicRemote().sendText(message);
            session.getBasicRemote().sendText(msg);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
 
    /**
     * The user closes the connection.
     * 
     * Note: you can't send messages to the client from this method
     */
    @OnClose
    public void onClose(Session session){
        //System.out.println("Session " +session.getId()+" has ended");
    	String ipAddr = httpSession.getAttribute("remoteHost").toString();
    	
    	System.out.println("Session With IP :" +ipAddr+" has ended");
    }
}
