package com.stl.server;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.websocket.HandshakeResponse;
import javax.websocket.server.HandshakeRequest;
import javax.websocket.server.ServerEndpointConfig;

public class ServletAwareConfig extends ServerEndpointConfig.Configurator {

    @Override
    public void modifyHandshake(ServerEndpointConfig config, HandshakeRequest request, HandshakeResponse response) {
        HttpSession httpSession = (HttpSession) request.getHttpSession();
        
        //System.out.println(request.getRequestURI());
        Map<String,List<String>> map = request.getHeaders();
        String host = "" ;
        for (String key : map.keySet()) {
            System.out.println("Key = " + key);
            if(key.equals("host")){
            	host = map.get("host").get(0);
            }
        }
        //System.out.println("host: "+host);
        //iterating over values only
        for (List<String> value : map.values()) {
            System.out.println("Values = " + value);
            for(int i=0;i<value.size();i++)
            	System.out.println(value.get(i));
        }
        if(null!=httpSession)
        	config.getUserProperties().put("httpSession", httpSession);
        //config.getUserProperties().put("requestURI", request.getRequestURI());
        config.getUserProperties().put("host", host);
    }

}
